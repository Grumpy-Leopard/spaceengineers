#region preamble
#if DEBUG
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers
{
    public sealed class ProgramDeepMineShort : MyGridProgram
    {
#endif
        #endregion preamble
        /*
         * Script taken from Workshop Blueprint, all rights belong to original owner
         * http://steamcommunity.com/sharedfiles/filedetails/?id=898042052
         * Used for inspiration
         */

        public void Main(string argument)
        {
            IMyMotorStator moottori = GridTerminalSystem.GetBlockWithName("RotorV") as IMyMotorStator;
            IMyTimerBlock ajastin = GridTerminalSystem.GetBlockWithName("Timer Block 2") as IMyTimerBlock;
            float angle = moottori.Angle * (180f / 3.14159f);
            if (Convert.ToUInt32(moottori.CustomData) == 1)
            {
                moottori.SetValueFloat("LowerLimit", angle);
            }
            else if (Convert.ToUInt32(moottori.CustomData) == 0)
            {
                moottori.SetValueFloat("UpperLimit", angle);
            }
            ajastin.GetActionWithName("Start").Apply(ajastin);
        }
        #region postamble
#if DEBUG
    }
}
#endif
#endregion postamble
