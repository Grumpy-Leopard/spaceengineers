﻿#region preamble
#if DEBUG
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers
{
    public sealed class Program : MyGridProgram
    {
#endif
        #endregion preamble
        //=======================================================================
        //////////////////////////BEGIN//////////////////////////////////////////
        //=======================================================================

        string nametag = "DeepMine"; // Default (TODO: Allow change with argument)
        bool DebugMode = true; // For testing purposes

        List<IMyPistonBase> Pistons = new List<IMyPistonBase>();
        IMyMotorAdvancedStator RotorH;
        IMyMotorAdvancedStator RotorV;
        IMyTimerBlock Timer;
        IMyShipDrill Drill;
        List<IMyShipConnector> Ejectors = new List<IMyShipConnector>();
        List<IMyCargoContainer> Containers = new List<IMyCargoContainer>();
        float Cargo = 0;
        float CargoMax = 0;
        enum States { Idle, Initializing, Working, Paused, Resetting, Complete, Error }
        States State = States.Idle;
        int TickCount = 0;
        List<IMyTerminalBlock> StorageBlocks = new List<IMyTerminalBlock>();

        public Program()
        {
            State = States.Initializing;
            if ("" != Me.CustomData.ToString())
            {
                nametag = Me.CustomData.ToString();
                if (!Me.CustomName.Contains("["+nametag+"]"))
                {
                    Me.CustomName += " [" + nametag + "]";
                }
            }
            PrintDebug("Using Nametag: " + nametag);
            SetTimerBlock();

            if(States.Error != State)
            {
            SetPistons();
            SetRotors();
            SetDrill();
            SetEjectors();
            SetCargo();
            }
       }

        public void Main(string args)
        {
            TickCount++;

            switch (args)
            {
                case "RESET":
                    State = States.Resetting;
                    TickCount = 0;
                    Reset();
                    break;

                case "PAUSE":
                    Pause();
                    break;

                default:
                    Update();
                    break;
            }
        }


        public void Update()
        {
            switch (State)
            {
                case States.Idle:
                    Timer.StartCountdown();
                    break;

                case States.Initializing:
                    Initialize();
                    Timer.StartCountdown();
                    break;

                case States.Working:
                    Timer.StartCountdown();
                    break;

                case States.Paused:
                    Timer.StartCountdown();
                    break;

                case States.Resetting:
                    PrintDebug("Resetting (" + TickCount + ") ...");
                    Reset();
                    break;

                case States.Complete:
                    Timer.StopCountdown();
                    PrintDebug("Completed: " + TickCount);
                    TickCount = 0;
                    break;

                case States.Error:
                    PrintError("Run cancelled: Error Flag\n" +
                        "Please recompile.");
                    break;

                default:
                    PrintError("Invalid State!");
                    break;
            }
        }

        public void Reset()
        {
            bool WaitingPistons = false;
            foreach (IMyPistonBase piston in Pistons)
            {
                if (0 < piston.Velocity)
                {
                    piston.Velocity = (piston.Velocity * -1);
                    WaitingPistons = true;
                    PrintDebug("Reversing piston.");
                }
                if (0 < piston.MinLimit)
                {
                    piston.MinLimit = 0;
                    WaitingPistons = true;
                    PrintDebug("Piston MinLimit Adjusted.");
                }
                if (piston.CurrentPosition > piston.MinLimit)
                {
                    WaitingPistons = true;
                    PrintDebug("Piston moving...");
                }
            }
            if (!WaitingPistons)
            {
                bool WaitingRotors = false;
                if (RotorH.Angle != RotorH.LowerLimit)
                {
                    RotorH.TargetVelocity = -0.1f;
                    WaitingRotors = true;
                    PrintDebug("Horizontal rotor retracting...");
                }
                if (RotorV.Angle != RotorV.LowerLimit)
                {
                    RotorV.TargetVelocity = -0.1f;
                    WaitingRotors = true;
                    PrintDebug("Vertical rotor retracting...");
                }
                if (!WaitingRotors)
                {
                    State = States.Complete;
                }
            }
            Timer.StartCountdown();
        }

        public void Pause()
        {
            if (States.Paused == State)
            {
                Initialize();
                // Unpause
                // Run init, resume positions
            }
            else
            {
                State = States.Paused;
                // Pause
                // Save positions
            }
        }

        public void Initialize()
        {
            State = States.Initializing;
            // Init stuff - called when resuming/etc
        }

        public void SetCargo()
        {
            List<IMyTerminalBlock> TempBlockGroup = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + "]"));
            if (0 < TempBlockGroup.Count)
            {
                int CargoCount = 0;
                Cargo = CargoMax = 0;
                foreach (IMyCargoContainer cargo in TempBlockGroup)
                {
                    CargoMax += (float)cargo.GetInventory().MaxVolume;
                    Cargo += (float)cargo.GetInventory().CurrentVolume;
                    Containers.Add(cargo);
                    CargoCount++;
                }
                PrintDebug("Found cargo containters: " + CargoCount);
                PrintDebug("Cargo: " + Cargo + "/" + CargoMax);
            }
            else
            {
                PrintError("ERR-Cargo: No cargo containers for ore.");
            }

        }
        public void SetEjectors()
        {
            List<IMyTerminalBlock> TempBlockGroup = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + "]"));
            if (0 < TempBlockGroup.Count)
            {
                int EjectorCount = 0;
                foreach (IMyShipConnector ejector in TempBlockGroup)
                {
                    ejector.ThrowOut = true;
                    Ejectors.Add(ejector);
                    EjectorCount++;
                }
                PrintDebug("Found ejectors: " + EjectorCount);
            }
            else
            {
                PrintError("ERR-Eject: No connectors for stone ejection.");
            }
        }
        public void SetDrill()
        {
            List<IMyTerminalBlock> TempBlockGroup = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyShipDrill>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + "]"));
            if (1 == TempBlockGroup.Count)
            {
                Drill = (IMyShipDrill)TempBlockGroup.First();
            }
            else
            {
                PrintError("ERR-Drill: Must be exactly one drill, found: "+TempBlockGroup.Count);
            }
        }
        public void SetRotors()
        {
            List<IMyTerminalBlock> TempBlockGroup = new List<IMyTerminalBlock>();

            GridTerminalSystem.GetBlocksOfType<IMyMotorAdvancedStator>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + ":H]"));
            if (1 == TempBlockGroup.Count)
            {
                RotorH = (IMyMotorAdvancedStator)TempBlockGroup.First();
                RotorH.TargetVelocity = 0f;
                if (0f < MathHelper.ToDegrees(RotorH.Angle))
                {
                    RotorH.UpperLimit = 360f;
                    RotorH.LowerLimit = 0f;
                    PrintDebug("Horzontal rotor configured.");
                }
                else
                {
                    PrintError("ERR-RotorH: Horizontal rotor out of bounds.");
                }
            }
            else
            {
                PrintError("ERR-RotorH: Must be exactly one horizontal rotor, found: " + TempBlockGroup.Count);

            }

            GridTerminalSystem.GetBlocksOfType<IMyMotorAdvancedStator>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + ":V]"));
            if (1 == TempBlockGroup.Count)
            {
                RotorV = (IMyMotorAdvancedStator)TempBlockGroup.First();
                RotorV.TargetVelocity = 0f;
                if (20f < MathHelper.ToDegrees(RotorV.Angle) && MathHelper.ToDegrees(RotorV.Angle) < 340f)
                {
                    RotorV.LowerLimit = 20f;
                    RotorV.UpperLimit = 340f;
                    PrintDebug("Vertical rotor configured.");
                }
                else
                {
                    PrintError("ERR-RotorV: Vertical rotor out of bounds.");
                }
            }
            else
            {
                PrintError("ERR-RotorV: Must be exactly one vertical rotor, found: "+TempBlockGroup.Count);
            }

        }
        public void SetPistons()
        {
            List<IMyTerminalBlock> TempBlockGroup = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyPistonBase>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + "]"));

            if (0 < TempBlockGroup.Count)
            {
                int PistonCount = 0;
                foreach (IMyPistonBase piston in TempBlockGroup)
                {
                    piston.Velocity = 0;
                    Pistons.Add(piston);
                    PistonCount++;
                }
                PrintDebug("Found pistons: " + PistonCount);
            }
            else
            {
                PrintError("ERR-Piston: No pistons found.");
            }
        }

        public void SetTimerBlock()
        {
            List<IMyTimerBlock> TempBlockGroup = new List<IMyTimerBlock>();
            GridTerminalSystem.GetBlocksOfType<IMyTimerBlock>(TempBlockGroup);
            TempBlockGroup = TempBlockGroup.FindAll(block => block.CustomName.Contains("[" + nametag + "]"));
            if (1 == TempBlockGroup.Count)
            {
                Timer = (IMyTimerBlock)TempBlockGroup.First();
            }
            else
            {
                PrintError("ERR-Timer: Must be exactly one timer block, found: "+TempBlockGroup.Count);
            }
        }

        public void PrintError(string message)
        {
            State = States.Error;
            Echo(message + "\n");
            // TODO: Allow LCD for errors
        }

        public void PrintDebug(string message)
        {
            if (DebugMode)
            {
                Echo(message + "\n");
                // TODO: Allow LCD for debug
            }
        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means.

            // This method is optional and can be removed if not
            // needed.
        }

        //=======================================================================
        //////////////////////////END////////////////////////////////////////////
        //=======================================================================
        #region postamble
#if DEBUG
    }
}
#endif
#endregion postamble