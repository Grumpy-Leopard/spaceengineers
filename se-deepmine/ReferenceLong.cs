#region preamble
#if DEBUG
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers
{
    public sealed class ProgramTemplate : MyGridProgram
    {
#endif
        #endregion preamble
        /*
         * Script taken from Workshop Blueprint, all rights belong to original owner
         * http://steamcommunity.com/sharedfiles/filedetails/?id=898042052
         * Used for inspiration
         */

        float lowlimit = 20.00f;
        float uplimit = 340.00f;
        int Motor(string name, float angle, float rpm)
        {
            IMyMotorStator motor = GridTerminalSystem.GetBlockWithName(name) as IMyMotorStator;
            float cangle = 0.00f;
            int limithit = 0;

            if (angle >= 360)
                angle = angle - 360;
            else if (angle < 0)
                angle = angle + 360;

            if (angle < lowlimit)
            {
                angle = lowlimit;
                limithit = 1;
            }

            if (angle > uplimit)
            {
                angle = uplimit;
                limithit = 1;
            }

            if (motor.TargetVelocity > 0)
                cangle = motor.GetValue<float>("UpperLimit");
            else if (motor.TargetVelocity < 0)
                cangle = motor.GetValue<float>("LowerLimit");

            if (angle > cangle)
            {
                motor.SetValueFloat("UpperLimit", angle);
                motor.CustomData = "1";
                motor.SetValueFloat("Velocity", rpm);
            }
            else if (angle < cangle)
            {
                motor.SetValueFloat("LowerLimit", angle);
                motor.CustomData = "0";
                motor.SetValueFloat("Velocity", -rpm);
            }
            return limithit;
        }

        void SetPistonPos(IMyPistonBase piston, float dist)
        {
            piston.MaxLimit = dist;
            piston.MinLimit = dist;
        }
        void MovePiston(string name, float dist, float speed)
        {

            IMyPistonBase piston1 = GridTerminalSystem.GetBlockWithName(name + "1") as IMyPistonBase;
            IMyPistonBase piston2 = GridTerminalSystem.GetBlockWithName(name + "2") as IMyPistonBase;
            IMyPistonBase piston3 = GridTerminalSystem.GetBlockWithName(name + "3") as IMyPistonBase;

            float mindist = 18.75f;
            dist = dist - mindist;
            if (dist < 0)
                dist = 0.00f;

            if (piston1.MaxLimit > dist / 3.0f)
            {
                piston1.Velocity = -speed;
                piston2.Velocity = -speed;
                piston3.Velocity = -speed;
            }
            else if (piston1.MaxLimit < dist / 3.0f)
            {
                piston1.Velocity = speed;
                piston2.Velocity = speed;
                piston3.Velocity = speed;
            }
            SetPistonPos(piston1, dist / 3.0f);
            SetPistonPos(piston2, dist / 3.0f);
            SetPistonPos(piston3, dist / 3.0f);

        }
        // pii 3.14159f 
        float RotationRadius(float angle, float dist)
        {

            return (float)(dist * Math.Sin(angle * (3.14159f / 180f)));
        }
        void phase(int x, int y, int z)
        {
            float MovementMultiplier = 1.00f; //Metres per step 
            float SpeedMultiplier = 1.25f; // M/s "DRILL" 
            IMyTimerBlock ajastin = GridTerminalSystem.GetBlockWithName("Timer Block") as IMyTimerBlock;
            IMyMotorStator RotorH = GridTerminalSystem.GetBlockWithName("RotorH") as IMyMotorStator;
            float dist = 18.75f + z * MovementMultiplier;
            float rvrpm = 60 / (dist * 2 * 3.14159f) * SpeedMultiplier;

            if (y == 0)
            {
                MovePiston("Piston ", dist, 1f / 3f);
                RotorH.SetValueFloat("Velocity", 0.00f);
                Motor("RotorV", 180, rvrpm);
                ajastin.TriggerDelay = 60 / rvrpm / (360 / (180 - lowlimit));
                y++;
            }
            else if (y > 0)
            {
                int ret = Motor("RotorV", 180 - 360 / ((dist * 2 * 3.14159f) / (MovementMultiplier * y)), rvrpm);
                RotorH.SetValueFloat("Velocity", 60f / (RotationRadius(180 - 360 / (dist * 2 * 3.14159f / (MovementMultiplier * y)), dist) * 2 * 3.14159f) * SpeedMultiplier);
                ajastin.TriggerDelay = 60 / RotorH.TargetVelocity;

                if (1 == ret)
                {
                    y = 0;
                    z++;
                }
                else
                {
                    y++;
                }
            }
            ajastin.GetActionWithName("Start").Apply(ajastin);
            GridTerminalSystem.GetBlockWithName("Piston 1").CustomData = x.ToString();
            GridTerminalSystem.GetBlockWithName("Piston 2").CustomData = y.ToString();
            GridTerminalSystem.GetBlockWithName("Piston 3").CustomData = z.ToString();
        }
        public void Main(string argument)
        {

            IMyMotorStator moottori = GridTerminalSystem.GetBlockWithName("Rotor 2") as IMyMotorStator;

            string pistonstring = GridTerminalSystem.GetBlockWithName("Piston 1").CustomData;
            int x = (int)Convert.ToUInt32(pistonstring);
            pistonstring = GridTerminalSystem.GetBlockWithName("Piston 2").CustomData;
            int y = (int)Convert.ToUInt32(pistonstring);
            pistonstring = GridTerminalSystem.GetBlockWithName("Piston 3").CustomData;
            int z = (int)Convert.ToUInt32(pistonstring);

            phase(x, y, z);
        }
        #region postamble
#if DEBUG
    }
}
#endif
#endregion postamble