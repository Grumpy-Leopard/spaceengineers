﻿#region preamble
#if DEBUG
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

namespace SpaceEngineers
{
    public sealed class Program : MyGridProgram
    {
#endif
        #endregion preamble
        //=======================================================================
        //////////////////////////BEGIN//////////////////////////////////////////
        //=======================================================================

        enum State { Paused, Waiting, Moving };
        enum Order { Idle, SetHome, FlyTo }
        int RunCount = 0;
        Vector3D position = new Vector3D(0, 0, 0);

        public Program()
        {
            // The constructor, called only once every session and
            // always before any other method is called. Use it to
            // initialize your script.

            // Ensure block starts offline on recompile
            // TODO: Ensure this also resets to manual control/hover
            State currentState = State.Paused;

            // Force position update
            position = Me.GetPosition();
        }

        public void Main(string args)
        {
            // The main entry point of the script, invoked every time
            // one of the programmable block's Run actions are invoked.

            // The method itself is required, but the argument above
            // can be removed if not needed.

            RunCount++;
            if (args != "")
            {
                switch (args)
                {
                    case "Reset":
                        {
                            // TODO: Reset all controls to manual
                            break;
                        }
                    case "SetHome":
                        {
                            // TODO: Set home GPS position and save
                            break;
                        }
                    case "SetPoint":
                        {
                            // TODO: Set destination GPS and plot course
                            break;
                        }
                    default:
                        break;
                }

            }

            // Get position and speed
            Vector3D current_position = Me.GetPosition();
            double speed = ((current_position - position) * 60).Length(); // Only accurate on self-trigger!
            position = current_position;


        }

        public void Save()
        {
            // Called when the program needs to save its state. Use
            // this method to save your state to the Storage field
            // or some other means.

            // This method is optional and can be removed if not
            // needed.
        }

        public class Ship
        {

        }

        //=======================================================================
        //////////////////////////END////////////////////////////////////////////
        //=======================================================================
        #region postamble
#if DEBUG
    }
}
#endif
#endregion postamble